FROM redisfab/rmbuilder:6.0.9-x64-bionic as builder

# Build the source
ADD . /
WORKDIR /
RUN set -ex;\
    make clean; \
    make all -j 4; \
    make test;

# Package the runner
FROM registry.redhat.io/rhel9/redis-6:1-129
MAINTAINER docker-dev@soterosoft.com

USER root

ENV LIBDIR /opt/bitnami/redis/modules
WORKDIR /bitnami/redis/data
RUN set -ex;\
    mkdir -p "$LIBDIR";
COPY --from=builder /redisbloom.so "$LIBDIR"

RUN chown -R 1001:0 /opt/bitnami/redis/modules/redisbloom.so && \
    chmod -R g=u /opt/bitnami/redis/modules/redisbloom.so && \
    chown -R 1001:0 /bitnami/redis/data/ && \
    chmod -R g=u /bitnami/redis/data/

USER 1001

ARG RELEASE_VERSION
LABEL name="SoteroSoft/RedisBloom" \
      vendor="SoteroSoft" \
      version="${RELEASE_VERSION}" \
      summary="SoteroSoft Redis BloomPI" \
      description="This image will deploy a Redis Bloom needed for Detect API"

COPY licenses /licenses

CMD [ "run-redis" , "--loadmodule", "/opt/bitnami/redis/modules/redisbloom.so" ]
