FROM redisfab/rmbuilder:6.0.9-x64-bionic as builder
# Build the source
ADD . /
WORKDIR /
RUN set -ex; \
    make clean; \
    make all -j 4; \
    make test;

# Package the runner
FROM bitnami/redis:7.4.0
ENV ALLOW_EMPTY_PASSWORD yes
RUN sed -i '/^bind/s/bind.*/bind 0.0.0.0/' /opt/bitnami/redis/etc/redis.conf
ENV LIBDIR /opt/bitnami/redis/modules
WORKDIR /bitnami/redis/data
RUN set -ex; \
    mkdir -p "$LIBDIR"
COPY --from=builder /redisbloom.so "$LIBDIR"

ARG RELEASE_VERSION
LABEL name="SoteroSoft/RedisBloom" \
      vendor="SoteroSoft" \
      version="${RELEASE_VERSION}" \
      summary="SoteroSoft Redis BloomPI" \
      description="This image will deploy a Redis Bloom needed for Detect API"

COPY licenses /licenses

CMD [ "/opt/bitnami/scripts/redis/run.sh" , "--loadmodule", "/opt/bitnami/redis/modules/redisbloom.so"]
#CMD ["redis-server", "--loadmodule", "/usr/lib/redis/modules/redisbloom.so"]
